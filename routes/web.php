<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardSuratController;
use App\Http\Controllers\Datamaster\DashboardController;
use App\Http\Controllers\Datamaster\KaryawanController;
use App\Http\Controllers\Datamaster\JabatanController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
Route::get('registration', [RegistrationController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [RegistrationController::class, 'customRegistration'])->name('register.custom');
Route::get('dashboardsurat', [DashboardSuratController::class, 'index'])->name('dashboardsurat')->middleware('auth');
Route::prefix('datamaster')->group(function(){
	Route::get('/dashboard', [App\Http\Controllers\Datamaster\DashboardController::class, 'index'])->name('datamaster.dashboard');
	Route::get('/karyawan', [App\Http\Controllers\Datamaster\KaryawanController::class, 'index'])->name('datamaster.karyawan');
	Route::get('/tambahdatakaryawan', [App\Http\Controllers\Datamaster\KaryawanController::class, 'create'])->name('datamaster.tambahdatakaryawan');
	Route::get('/editdatakaryawan', [App\Http\Controllers\Datamaster\KaryawanController::class, 'edit'])->name('datamaster.editdatakaryawan');
	Route::get('/jabatan', [App\Http\Controllers\Datamaster\JabatanController::class, 'index'])->name('datamaster.jabatan');
	Route::get('/tambahdatajabatan', [App\Http\Controllers\Datamaster\JabatanController::class, 'create'])->name('datamaster.tambahdatajabatan');
	Route::get('/editdatajabatan', [App\Http\Controllers\Datamaster\JabatanController::class, 'edit'])->name('datamaster.editdatajabatan');
});