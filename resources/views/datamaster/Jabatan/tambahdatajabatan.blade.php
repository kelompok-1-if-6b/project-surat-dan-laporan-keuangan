@extends('layouts.home.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<br>
			<h2><center>Tambah Data Jabatan</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			<form action="" method="POST">
				@csrf
				<div class="form-group">
					<label>ID Jabatan</label>
					<input type="text" name="id_jabatan" class="form-control" placeholder="Id Jabatan">
				</div>
				<div class="form-group">
					<label>Nama Jabatan</label>
					<input type="text" name="nama_jabatan" class="form-control" placeholder="Nama Jabatan">
				</div>
				
				
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			</div>
			<br>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection