<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JabatanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('datamaster.jabatan.jabatan');
    }

    public function create()
    {

        return view('datamaster.jabatan.tambahdatajabatan');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_jabatan' => 'required',
            'nama_jabatan' => 'required',
        ]);

        $aktakematian = Aktakematian::create([
        'id_jabatan' => $request->id_jabatan,
        'nama_jabatan' => $request->namajabatan,
        ]);

        if($aktakematian){
        //redirect dengan pesan sukses
            return redirect()->route('admin.jabatan')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createjabatan')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit()
    {

        return view('datamaster.jabatan.editdatajabatan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_akte_kematian')->where('id_akta_kematian',$request->id_akta_kematian)->update([
	    'id_akta_kematian' => $request->id_akta_kematian,
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'tempat_kematian' => $request->tempat_kematian,
        'tanggal_kematian' => $request->tanggal_kematian,
        'id_jeniskelamin' => $request->id_jeniskelamin,
        'anak_ke' => $request->anak_ke,
        'id_agama' => $request->id_agama
        ]);

        return redirect('/admin/aktakematian')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id_akta_kematian)
    {
        DB::table('tb_akte_kematian')->where('id_akta_kematian', $id_akta_kematian)->delete();

        return redirect ('/admin/aktakematian')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
