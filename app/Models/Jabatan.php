<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jabatan extends Model
{
    public $table = "";
    protected $primaryKey = '';
    public $timestamps = false;

    protected $fillable = [
        'id_jabatan', 'nama_jabatan'
    ];

}